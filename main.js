const jsdom = require("jsdom");

async function getRSSLinks(html) {
  const dom = new jsdom.JSDOM(html, { contentType: "text/html" });
  const document = dom.window.document;
  const links = Array.from(document.getElementsByTagName("link"));
  const rssLinks = links.filter((link) => link.type === "application/rss+xml");
  return rssLinks.map((link) => link.href);
}

async function testMostCommonURLs(url) {
  const commonURLs = ["rss", "feed"];

  const validURLs = [];
  for (const commonURL of commonURLs) {
    const cURL = new URL(commonURL, url);
    const res = await fetch(cURL);
    if (res.status === 200) {
      validURLs.push(cURL);
    }
  }
  return validURLs;
}

async function getReport(urls) {
  const report = [];
  for (const url of urls) {
    try {
      const res = await fetch(url);
      const html = await res.text();

      // links found in home page
      let linksFromDOM = (await getRSSLinks(html)).map((link) => {
        try {
          return new URL(link); // if link is not an absolute path an exception will raise
        } catch (_) {
          return new URL(link, url); // join `url` and `link` to create an absolute path
        }
      });

      // most common url for feeds (only if in home page there is no link to them)
      const alternativeLinks = linksFromDOM.length > 0 ? [] : await testMostCommonURLs(url);

      const links = linksFromDOM.concat(alternativeLinks);
      
      report.push({ url, links });
    } catch (error) {
      report.push({ url, error });
    }
  }
  return report;
}

function printReport(report) {
  console.log();
  report.forEach((elem) => {
    if (elem.error) {
      console.log(`Error for url: ${elem.url}`);
      console.log(elem.error);
    } else if (elem.links && elem.links.length > 0) {
      console.log(`RSS links found for url: ${elem.url}`);
      console.log(elem.links.join("\n"));
    } else {
      console.log(`no RSS link found for url: ${elem.url}`);
    }
    console.log();
  });
}

function main() {
  const args = process.argv.slice(2);

  if (args.length < 1) {
    console.log("usage: main.js <url> ...");
    process.exit(1);
  }

  const urls = args.map((arg) => {
    try {
      return new URL(arg);
    } catch (e) {
      console.error(`invalid argument: ${e.message}`);
      process.exit(2);
    }
  });

  getReport(urls)
    .then((report) => printReport(report))
    .catch((e) => {
      console.error(`error while reaching the url: ${e.message}`);
      process.exit(3);
    });
}

main();
